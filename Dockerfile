# Ubuntu 22.04.1 LTS
FROM ubuntu@sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00

MAINTAINER "Hatchet" "support@hatchet.com.au"

COPY .docker/sources.list /etc/apt/sources.list

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    curl \
    unzip \
    python3 \
    rsync \
    build-essential \
    git \
    libpng-dev \
    openssh-client \
    software-properties-common

# NodeJS
RUN curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install -y nodejs

# Install NPM and Yarn
RUN npm install -g npm
RUN npm install -g yarn@latest

# Set timezone
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

# PHP AND DEPENDENCIES
RUN add-apt-repository ppa:ondrej/php
RUN apt-get install -y php8.2-fpm \
    php8.2-cli \
    php8.2-curl \
    php8.2-common \
    php8.2-zip \
    php8.2-gd \
    php8.2-soap \
    php8.2-exif \
    php8.2-mysqli \
    php8.2-bcmath \
    php8.2-intl \
    php8.2-sockets \
    php8.2-xml \
    php8.2-mbstring \
    php8.2-mailparse \
    php8.2-sqlite3 \
    php8.2-imagick

# INSTALL COMPOSER
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# CLEANUP
RUN rm -rf /tmp/* \
    && rm -rf /var/list/apt/* \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean